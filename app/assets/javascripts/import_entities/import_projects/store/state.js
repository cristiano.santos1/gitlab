export default () => ({
  provider: '',
  repositories: [],
  customImportTargets: {},
  isLoadingRepos: false,
  ciCdOnly: false,
  filter: '',
  pageInfo: {
    page: 0,
  },
});
